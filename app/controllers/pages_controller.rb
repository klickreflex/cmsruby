class PagesController < ApplicationController
  def show
    @page = Page.find params[:id]
  end

  def homepage 
    @page = Page.first
  end 
end